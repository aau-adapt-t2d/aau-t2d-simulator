%Define a patient class 
classdef T2Dp < handle
   properties
   Param = paramClass
   %Initial values:
   %Glucose concentration in the periphery central compartment:
   GBPC0=7/0.0555; % in mg/dl
    
   %Insulin concentraion in the in periphery compartment: 
   IBPF0=1; % in mU/l
   
   %Basal rates (All basal rates except for rHGP and rPIR
   % since they are calculated from the others):
   brates=struct('rBGU',70,'rRBCU',10,'rGGU',20,'rPGU',35,'rHGU',20);
   %Simulation parameters:
        Nd = 4 %Number of simulation days
        dt = 1;%Simulation step sizes for the inputs (in minutes)
        %Nmin; %Number of minutes for the simulation
        %T; %Time vector in minutes for the inputs
        
        Meals;% in grams of carbohydrates
        
        %Long acting insulin:
        Ula;% in Units
        
        %Fast acting insulin:
        Ufa;% in Units
        
        %Heart rate difference from base (HRb):
        HRd;% in bpm
        
        %Metformin doses: 
        Um;% in mg
        
        %Vildagliptin:
        Uv;% in mmol
        
        %Stress: 
        Stress;% from 0 to 1 
       
       %Simulated states:
       X=struct();
       %Simulated time:
       time;
       %Final simulation states:
       Xend;
       % Initial conditions if simulation in loop:
       X0sim = [];
       
       %Last Meal state:
       LastMeal = 0;
   end
   
   %Dependent properties:
   properties (Dependent)
       %Closed loop mode:
       CLmode;
       %Initial conditions:
       X0;
       %Basal values:
       basal;
       %Time of the simulation:
       T;
       %Number of data points in minutes:
       Nmin;
   end
   
   properties (Dependent,Hidden)
          %Dependent Inputs:
       MealD;
       
       %Long acting insulin:
       UlaD;% in Units
        
       %Fast acting insulin:
       UfaD;% in Units
        
       %Heart rate difference from base (HRb):
       HRdD;% in bpm
        
       %Metformin doses: 
       UmD;% in mg
        
       %Vildagliptin:
       UvD;% in mg
        
       %Stress: 
       StressD;% from 0 to 1 
   end
   
   methods
       %Constructor: 
       function obj = T2Dp(Param,GBPC0,IBPF0,brates,Nd,dt)
            if(nargin>0)
                if not(isempty(Param)) 
                    obj.Param = Param;
                end
                if not(isempty(GBPC0))
                obj.GBPC0 = GBPC0;
                end
                if not(isempty(IBPF0))
                obj.IBPF0 = IBPF0;
                end
                if not(isempty(brates))
                obj.brates = brates;
                end
                if not(isempty(Nd))
                obj.Nd = Nd;
                end
                if not(isempty(dt))
                obj.dt = dt;
                end
            end
            %obj.brates = brates;
            
            %obj.Nmin = obj.Nd*(obj.dt*24*60);
            %obj.T = 0:(obj.Nmin-1);
            obj.Meals = perday(obj,[],[]);
            obj.Ula = perday(obj,[],[]);
            obj.Ufa = perday(obj,[],[]);
            obj.Um = perday(obj,[],[]);
            obj.Uv = perday(obj,[],[]);
            obj.Stress = perday(obj,[],[]);
            obj.HRd = perday(obj,[],[]);
       end
       
       %Set:
      function  set.Param(obj,Param)
         obj.Param = Param;
      end
      
      function  set.GBPC0(obj,GBPC0)
         obj.GBPC0 = GBPC0;
      end 
      
      function  set.IBPF0(obj,IBPF0)
         obj.IBPF0 = IBPF0;
      end
      
      function  set.brates(obj,brates)
         obj.brates = brates;
      end
      
      function  set.Nd(obj,Nd)
         obj.Nd = Nd;
      end
      
      function  set.dt(obj,dt)
         obj.dt = dt;
      end
      
      function  set.X0(obj,X0)
         obj.X0 = X0;
      end
      
      function  set.Meals(obj,Meals)
         obj.Meals = Meals;
      end
      
      function  set.Ula(obj,Ula)
         obj.Ula = Ula;
      end
      
      function  set.Ufa(obj,Ufa)
         obj.Ufa = Ufa;
      end
      
      function  set.Um(obj,Um)
         obj.Um = Um;
      end
      
      function  set.Uv(obj,Uv)
         obj.Uv = Uv;
      end
      
      function  set.HRd(obj,HRd)
         obj.HRd = HRd;
      end
      
      function  set.Stress(obj,Stress)
         obj.Stress = Stress;
      end
      
      function Nminv = get.Nmin(obj)
         Nminv = obj.Nd*(obj.dt*24*60);
      end
      
      function Tv = get.T(obj)
         Nminv = obj.Nd*(obj.dt*24*60);
         Tv = 0:(Nminv-1);
      end
      
%       function X0v = get.X0(obj)
%          [X0v,~,~] = BasaldefGCPFIPF(obj);
%       end
      
      function MealD = get.MealD(obj)
          if sum(obj.Meals) == 0
            MealD = zeros(1,floor(obj.Nd*24*60));
          else
              MealD = obj.Meals;
          end
      end
      
      function UlaD = get.UlaD(obj)
          if sum(obj.Ula) == 0
            UlaD = zeros(1,floor(obj.Nd*24*60));
          else
              UlaD = obj.Ula;
          end
      end
      
      function UfaD = get.UfaD(obj)
          if sum(obj.Ufa) == 0
            UfaD = zeros(1,floor(obj.Nd*24*60));
          else
              UfaD = obj.Ufa;
          end
      end
      
      function UmD = get.UmD(obj)
          if sum(obj.Um) == 0
            UmD = zeros(1,floor(obj.Nd*24*60));
          else
              UmD = obj.Um;
          end
      end
      
      function UvD = get.UvD(obj)
          if sum(obj.Uv) == 0
            UvD = zeros(1,floor(obj.Nd*24*60));
          else
              UvD = obj.Uv;
          end
      end
      
      function HRdD = get.HRdD(obj)
          if sum(obj.HRd) == 0
            HRdD = zeros(1,floor(obj.Nd*24*60));
          else
              HRdD = obj.HRd;
          end
      end
      
      function StressD = get.StressD(obj)
          if sum(obj.Stress) == 0
            StressD = zeros(1,floor(obj.Nd*24*60));
          else
              StressD = obj.Stress;
          end
      end
      
     function X0 = get.X0(obj)
         
          if isempty(obj.X0sim)
            X0 = BasaldefGCPFIPF(obj);
          else
            X0 = obj.X0sim;
          end
     end
      
     function CLmode = get.CLmode(obj)
         
          if isempty(obj.X0sim)
            CLmode = 0;
          else
            CLmode = 1;
          end
      end
      
      function basalv = get.basal(obj)
         [X0v,rates,SB] = BasaldefGCPFIPF(obj);
         basal0=struct();
         basal0.GPF=X0v(40,1); basal0.IPF=obj.IBPF0;...
         basal0.IL=X0v(29,1); basal0.GL=X0v(37,1);
         basal0.Gamma=X0v(41,1); basal0.SB=SB; basal0.GH=X0v(35,1); 
         basal0.IH=X0v(27,1);
         basal0.rPIR = rates(1);
         basal0.rBGU = rates(2);
         basal0.rRBCU = rates(3);
         basal0.rGGU = rates(4);
         basal0.rPGU = rates(5);
         basal0.rHGP = rates(6);
         basal0.rHGU = rates(7);
         basalv = basal0;
      end
       
       %To get the initial conditions in case of IPF and GPC given:
       [X0,rates,S]=BasaldefPF(obj)
       
       %Dynamics:
       dx=fode(obj,t, x, Dg, stressv, HRv, T, param, basal);
        
       %Function to define rectanglar inputs: 
       y = rectf(obj,c,t1,t2,T);
        
       %Function to define trapazoidal inputs: 
       y = Rampf(obj,c,t1,t2,t3,t4,T)
        
       %Function to define inputs perday:
       [y,yt] = perday(obj,tpd,upd);
        
       %Simulation function (ode45):
       [X,time]=Simulation(obj);
       
       %Faster Simulation function but less stable (ode15s):
       [X,time]=Simulationfast(obj);
       
       %Simulation with controller: 
       [X,time] = SimCont(obj,cont,Tc);
       
       %Measurement for SMBG: 
       MeasS = SMBGmeas(obj,Measpoints,mode);
       
       %CGM measurements:
       [MeasC,ycal,Tcgm] = CGMmeas(obj,TSMBG,cgmdt,model);
       
       %Draw Patients randomly with a specified set of parameters:
       %ThetaName is an M Cellarray of parameters' names (as strings) to be
       %selected randomly.
       %ThataS is the set of parameters provided as an M*L matrix where M
       %is the number of parameters and L is the number of different
       %patients. 
       DrawRandomPatient(obj,ThetaName,ThetaSet);
       
       %Choose long acting insulin:
       LongActingType(obj,Type);
       
       %Choose fast acting insulin:
       FastActingType(obj,Type);
   end
   
end
